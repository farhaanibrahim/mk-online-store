<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use Illuminate\Support\Str;

use App\Models\Product;

use App\Models\Category;

use App\Models\ProductImage;

use Session;



class ProductController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {

        if (Session::get('login')) {

            $products = Product::orderBy('id', 'desc')->get();

            foreach ($products as $product) {

                $category = Category::where('id', $product->product_category_id)->first();



                $product->product_category = $category->name;

            }

            $data['products'] = $products;



            return view('admin.product.index', $data);

        } else {

            

        }

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        if (Session::get('login')) {

            $categories = Category::all();

            $data['categories'] = $categories;



            return view('admin.product.create', $data);

        } else {

            return redirect('admin/login')->with('notification', 'Session expired');

        }

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

        if (Session::get('login')) {

            $featuredImage = time().'.'.$request->featured_image->extension();



            $request->featured_image->move(public_path('uploads/products'), $featuredImage);



            $product = Product::create([

                'product_name'  => $request->product_name,

                'product_category_id'  => $request->product_category,

                'description'   => $request->description,

                'stock' => $request->stock,

                'price' => $request->price,

                'image' => $featuredImage,

                'slug' => Str::slug($request->product_name, '-'),

            ]);

            

            foreach($request->file('more_image') as $img){

                $productImage = $img->getClientOriginalName();



                $img->move(public_path('uploads/products'), $productImage);



                ProductImage::create([

                    'product_id'    => $product->id,

                    'image' => $productImage

                ]);

            }



            return redirect('admin/product')->with('notification', 'Product saved');

        } else {

            return redirect('admin/login')->with('notification', 'Session expired');

        }

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        if (Session::get('login')) {

            $product = Product::where('id', $id)->first();



            //fetch product category

            $category = Category::where('id', $product->product_category_id)->first();



            //fetch product images

            $productImages = ProductImage::where('product_id', $product->id)->get();



            $product->product_category = $category->name;



            $product->images = $productImages;



            $data['product'] = $product;

            

            return view('admin.product.show', $data);

        } else {

            return redirect('admin/login')->with('notification', 'Session expired');

        }

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        if (Session::get('login')) {

            $product = Product::where('id', $id)->first();

            $categories = Category::all();



            //fetch product category

            $category = Category::where('id', $product->product_category_id)->first();



            //fetch product images

            $productImages = ProductImage::where('product_id', $product->id)->get();



            $product->product_category = $category->name;



            $product->images = $productImages;



            $data['product'] = $product;

            $data['categories'] = $categories;



            return view('admin.product.edit', $data);

        } else {

            return redirect('admin/login')->with('notification', 'Session expired');

        }

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

        if (Session::get('login')) {

            if ($request->featured_image == null) {

                $featuredImage = $request->old_featured_image;

    

                Product::where('id', $id)->update([

                    'product_name'  => $request->product_name,

                    'product_category_id'   => $request->product_category,

                    'description'   => $request->description,

                    'stock' => $request->stock,

                    'price' => $request->price,

                    'image' => $featuredImage,

                    'slug'  => Str::slug($request->product_name, '-')

                ]);

            } else {

                $featuredImage = time().'.'.$request->featured_image->extension();

    

                $request->featured_image->move(public_path('uploads/products'), $featuredImage);

    

                if (\File::exists(public_path('uploads/products/'.$request->old_featured_image))) {

                    \File::delete(public_path('uploads/products/'.$request->old_featured_image));

                }

    

                Product::where('id', $id)->update([

                    'product_name'  => $request->product_name,

                    'product_category_id'   => $request->product_category,

                    'description'   => $request->description,

                    'stock' => $request->stock,

                    'price' => $request->price,

                    'image' => $featuredImage,

                    'slug'  => $request->product_name

                ]);

            }

            

            if($request->file('more_image') !== null) 

            {

                foreach($request->file('more_image') as $img){

                    $productImage = $img->getClientOriginalName();

        

                    $img->move(public_path('uploads/products'), $productImage);

        

                    ProductImage::create([

                        'product_id'    => $id,

                        'image' => $productImage

                    ]);

                } 

            }

    

            return redirect('admin/product')->with('notification', 'Product updated');

        } else {

            return redirect('admin/login')->with('notification', 'Session expired');

        }

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)

    {

        if (Session::get('login')) {

            //get ProductImage from product_images table

            $productImages = ProductImage::where('product_id', $id)->get();

            foreach ($productImages as $productImage) {

                $productImage = $productImage->image;



                if (\File::exists(public_path('uploads/products/'.$productImage))) {

                    \File::delete(public_path('uploads/products/'.$productImage));

                }

            }



            //delete on product_images table

            ProductImage::where('product_id', $id)->delete();

            

            //get featured image

            $product = Product::where('id', $id)->first();

            $featuredImage = $product->image;

            

            //Remove file

            if (\File::exists(public_path('uploads/products/'.$featuredImage))) {

                \File::delete(public_path('uploads/products/'.$featuredImage));

            }



            //delete on products table

            Product::where('id', $id)->delete();



            return redirect('admin/product')->with('notification', 'Product deleted');

        } else {

            return redirect('admin/login')->with('notification', 'Session expired');

        }

    }



    public function remove_image($id)

    {

        if (Session::get('login')) {

            //fetch product image data

            $image = ProductImage::where('id', $id)->first();



            //get image name

            $productImage = $image->image;

            

            //Remove Image

            if (\File::exists(public_path('uploads/products/'.$productImage))) {

                \File::delete(public_path('uploads/products/'.$productImage));

            }



            //delete data on product_images table

            ProductImage::where('id', $id)->delete();



            return redirect()->back();

        } else {

            return redirect('admin/login')->with('notification', 'Session expired');

        }

    }



    public function show2($category_slug, $product_slug)

    {

        $product = Product::where('slug', $product_slug)->first();



        //fetch all categories

        $categories = Category::all();



        //fetch product category

        $category = Category::where('id', $product->product_category_id)->first();



        //fetch product images

        $productImages = ProductImage::where('product_id', $product->id)->get();



        $product->product_category = $category->name;



        $product->images = $productImages;



        $data['product'] = $product;

        $data['categories'] = $categories;

        

        return view('front.product', $data);

    }



    public function showProductByCategory($category_slug)

    {

        $category = Category::where('slug', $category_slug)->first();

        

        //fetch product

        $products = Product::where('product_category_id', $category->id)->orderBy('id', 'desc')->paginate(9);

        foreach ($products as $product) {

            $cat = Category::where('id', $product->product_category_id)->first();

            $product->category_slug = $cat->slug;

        }

        //fetch categories

        $categories = Category::all();

        

        $data['products'] = $products;

        $data['category_name'] = $category->name;

        $data['categories'] = $categories;



        return view('front.category', $data);

    }

    public function search(Request $request)
    {
        $products = Product::where('product_name', 'like', "%$request->search%")->orWhere('description', 'like', "%$request->search%")->get();

        foreach ($products as $product) {

            $cat = Category::where('id', $product->product_category_id)->first();

            $product->category_slug = $cat->slug;

        }
        
        $data['products'] = $products;
        $data['categories'] = Category::all();
        $data['keyword'] = $request->search;

        return view('front.product_search', $data);
    }

}