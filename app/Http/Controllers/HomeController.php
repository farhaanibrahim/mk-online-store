<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\Models\Category;

use App\Models\Product;

use App\Models\Carousel;

use App\Models\CompanyProfile;



class HomeController extends Controller

{

    public function index()

{

        $data['categories'] = Category::all();

        $data['carousels'] = Carousel::orderBy('id', 'desc')->get();

        $products = Product::orderBy('id', 'desc')->paginate(9);

        foreach ($products as $product) {

            $category = Category::where('id', $product->product_category_id)->first();

            $product->category_slug = $category->slug;

        }



        $data['products'] = $products;

        

        return view('front.home', $data);

    }



    public function profile()

    {
        $companyProfile = CompanyProfile::where('id', 1)->first();
        
        $data['companyProfile'] = $companyProfile;

        return view('front.about', $data);
    }

}

