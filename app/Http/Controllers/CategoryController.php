<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Str;
use Session;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Session::get('login')) {
            $categories = Category::all();
            $data['categories'] = $categories;

            return view('admin.category.index', $data);
        } else {
            return redirect('admin/login')->with('notification', 'Session expired');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Session::get('login')) {
            return view('admin.category.create');
        } else {
            return redirect('admin/login')->with('notification', 'Session expired');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Session::get('login')) {
            Category::create([
                'name'  => $request->category_name,
                'slug'  => Str::slug($request->category_name, '-')
            ]);
    
            return redirect('admin/product-categories')->with('notification', 'Data saved');
        } else {
            return redirect('admin/login')->with('notification', 'Session expired');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Session::get('login')) {
            $data = Category::where('id', $id)->first();
            $data['category'] = $data;

            return view('admin.category.edit', $data);
        } else {
            return redirect('admin/login')->with('notification', 'Session expired');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Session::get('login')) {
            Category::where('id', $id)->update([
                'name'  => $request->category_name,
                'slug'  => Str::slug($request->category_name, '-')
            ]);
    
            return redirect('admin/product-categories')->with('notification', 'Data updated');
        } else {
            return redirect('admin/login')->with('notification', 'Session expired');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Session::get('login')) {
            Category::where('id', $id)->delete();

            return redirect('admin/product-categories')->with('notification', 'Data deleted');
        } else {
            return redirect('admin/login')->with('notification', 'Session expired');
        }
    }
}
