<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CompanyProfile;

class CompanyProfileController extends Controller
{
    public function index()
    {
        $companyProfile = CompanyProfile::where('id', 1)->first();
        
        $data['companyProfile'] = $companyProfile;
        return view('admin.companyProfile.edit', $data);
    }

    public function update(Request $request)
    {
        CompanyProfile::where('id', 1)->update([
            'profile_content'   => $request->profile_content
        ]);

        return redirect('admin/company-profile')->with('notification', 'Company Profile Updated');
    }
}
