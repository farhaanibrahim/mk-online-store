<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use Hash;
use Session;

class AuthController extends Controller
{
    public function admin_login_view()
    {
        if (Session::get('login')) {
            return redirect('admin/home');
        } else {
            return view('admin.login');
        }
    }

    public function admin_login_process(Request $request)
    {
        $admin = Admin::where('username', $request->username)->first();
        if ($admin != null) {
            if (Hash::check($request->password, $admin->password)) {
                $request->session()->put('id', $admin->id);
                $request->session()->put('username', $admin->username);
                $request->session()->put('login', true);

                return redirect('admin/home');
            } else {
                return redirect('admin/login')->with('notification', 'Wrong Password');
            }
        } else {
            return redirect('admin/login')->with('notification', 'Username not found');
        }
        
    }

    public function admin_logout()
    {
        Session::flush();
        return redirect('admin/login')->with('notification', 'Logged Out');
    }
}
