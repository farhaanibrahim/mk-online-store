<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class HomeAdminController extends Controller
{
    public function index()
    {
        if (Session::has('login')) {
            return view('admin.home');
        } else {
            return redirect('admin/login')->with('notification', 'Session Expired');
        }
    }
}
