<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\Models\Carousel;

use Session;



class CarouselController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {

        if (Session::get('login')) {

            $carousels = Carousel::orderBy('id', 'desc')->get();

            $data['carousels'] = $carousels;



            return view('admin.carousel.index', $data);

        } else {

            return redirect('admin/login')->with('notification', 'Session Expired');

        }

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        if (Session::get('login')) {

            return view('admin.carousel.create');

        } else {

            return redirect('admin/login')->with('notification', 'Session Expired');

        }

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

        if (Session::get('login')) {

            $image = time().'.'.$request->image->extension();



            $request->image->move(public_path('uploads/carousels'), $image);



            Carousel::create([

                'image' => $image,

                'product_link'  => $request->product_link

            ]);



            return redirect('admin/carousel')->with('notification', 'Data saved');

        } else {

            return redirect('admin/login')->with('notification', 'Session expired');

        }

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        if (Session::get('login')) {

            $carousel = Carousel::where('id', $id)->first();



            $data['carousel'] = $carousel;



            return view('admin.carousel.edit', $data);

        } else {

            return redirect('admin/login')->with('notification', 'Session expired');

        }

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

        if (Session::get('login')) {

            if ($request->image == null) {

                $image = $request->old_image;

            } else {

                $image = time().'.'.$request->image->extension();

    

                $request->image->move(public_path('uploads/carousels'), $image);

                

                if (\File::exists(public_path('uploads/carousels/'.$request->old_image))) {

                    \File::delete(public_path('uploads/carousels/'.$request->old_image));

                }

            }

    

            Carousel::where('id', $id)->update([

                'image' => $image,

                'product_link'  => $request->product_link

            ]);

    

            return redirect('admin/carousel')->with('notification', 'Carousel updated');

        } else {

            return redirect('admin/login')->with('notification', 'Session expired');

        }

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)

    {

        if (Session::get('login')) {

            //FETCH DATA

            $carousel = Carousel::where('id', $id)->first();



            //Get image

            $image = $carousel->image;



            if (\File::exists(public_path('uploads/carousels/'.$image))) {

                \File::delete(public_path('uploads/carousels/'.$image));

            }



            Carousel::where('id', $id)->delete();



            return redirect('admin/carousel')->with('notification', 'Carousel image deleted');

        } else {

            return redirect('admin/login')->with('notification', 'Session expired');

        }

    }

}

