<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyProfile extends Model
{
    protected $table = 'profiles';

    protected $fillable = ['profile_content'];

    public $timestamps = false;
}
