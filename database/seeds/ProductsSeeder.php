<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Str;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = array(
            [
                'Product Item 1',
                'Product Item 2',
                'Product Item 3',
                'Product Item 4'
            ],
            [
                1,2,3,4
            ],
            [
                23, 50, 14, 12
            ],
            [
                300000, 123000, 250000, 15000
            ],
            [
                'vase.png',
                'camera.png',
                'drink.png',
                'watch.png'
            ]
        );

        $product_images = array(
            [
                1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4
            ],
            [
                'vase.png',
                'camera.png',
                'drink.png',
                'watch.png',
                'vase.png',
                'camera.png',
                'drink.png',
                'watch.png',
                'vase.png',
                'camera.png',
                'drink.png',
                'watch.png',
                'vase.png',
                'camera.png',
                'drink.png',
                'watch.png'
            ]
        );

        $countProduct = count($products[0]);
        $countProductImages = count($product_images[0]);

        for ($i=0; $i < $countProduct; $i++) { 
            DB::table('products')->insert([
                'product_name' => $products[0][$i],
                'product_category_id'  => $products[1][$i],
                'description'   => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Repellat molestiae in provident dolorem! Illum repellendus dolores unde vero. Atque porro dolorem laborum consectetur ad labore ab minus harum cum accusantium.',
                'stock' => $products[2][$i],
                'price' => $products[3][$i],
                'image' => $products[4][$i],
                'slug'  => Str::slug($products[0][$i], '-'),
                'created_at'    => Carbon::now('Asia/Jakarta'),
                'updated_at'    => Carbon::now('Asia/Jakarta'),
            ]);
        }

        for ($i=0; $i < $countProductImages; $i++) { 
            DB::table('product_images')->insert([
                'product_id'    => $product_images[0][$i],
                'image' => $product_images[1][$i]
            ]);
        }
    }
}
