<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = array('Category 1', 'Category 2', 'Category 3', 'Category 4');

        $arrayCount = count($categories);

        for ($i=0; $i < $arrayCount; $i++) { 
            DB::table('categories')->insert([
                'name' => $categories[$i],
                'slug'  => Str::slug($categories[$i], '-'),
            ]);
        }
    }
}
