<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CarouselsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $carousels = array(
            [
                'slider-dummy1.png',
                'slider-dummy2.png',
                'slider-dummy3.png'
            ],
            [
                '#','#','#'
            ]
        );

        $countArray = count($carousels[0]);

        for ($i=0; $i < $countArray; $i++) { 
            DB::table('carousels')->insert([
                'image' => $carousels[0][$i],
                'product_link'  => $carousels[1][$i],
                'created_at'    => Carbon::now('Asia/Jakarta'),
                'updated_at'    => Carbon::now('Asia/Jakarta')
            ]);
        }
    }
}
