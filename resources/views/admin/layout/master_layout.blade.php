<!DOCTYPE html>

<html lang="en">



<head>

    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Toko Pediailmu</title>

    <!-- Tell the browser to be responsive to screen width -->

    <meta name="viewport" content="width=device-width, initial-scale=1">



    <!-- Font Awesome -->

    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/fontawesome-free/css/all.min.css') }}">

    <!-- Ionicons -->

    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <!-- overlayScrollbars -->

    <link rel="stylesheet" href="{{ asset('assets/admin/dist/css/adminlte.min.css') }}">

    <!-- DataTabes -->
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">

    <!-- Google Font: Source Sans Pro -->

    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <link rel="icon" href="{{ asset('assets/front/img/logo.png') }}">

</head>



<body>

    <div class="wrapper">

        @include('admin.layout.header')





        @include('admin.layout.sidebar')



        <div class="content-wrapper">

            @yield('content')

        </div>



        @include('admin.layout.footer')

    </div>



    <!-- jQuery -->

    <script src="{{ asset('assets/admin/plugins/jquery/jquery.min.js') }}"></script>

    <!-- jQuery UI -->

    <script src="{{ asset('assets/admin/plugins/jquery-ui/jquery-ui.js') }}"></script>

    <!-- Bootstrap 4 -->

    <script src="{{ asset('assets/admin/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- AdminLTE App -->

    <script src="{{ asset('assets/admin/dist/js/adminlte.min.js') }}"></script>

    <!-- AdminLTE for demo purposes -->

    <script src="{{ asset('assets/admin/dist/js/demo.js') }}"></script>

    <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script>
    $(document).ready(function() {
        $('#productTable').DataTable();
    });
    </script>

</body>



</html>