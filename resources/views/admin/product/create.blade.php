@extends('admin.layout.master_layout')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Products</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/admin/home">Home</a></li>
                    <li class="breadcrumb-item active">Products</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Products Data</h3>
                </div>
                <div class="card-body">
                    <form action="{{ URL::to('admin/product/store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="">Product Name</label>
                            <input type="text" name="product_name" id="product_name" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="">Product Category</label>
                            <select name="product_category" id="product_category" class="form-control" required>
                                <option value="">Select Product Category</option>
                                @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Featured Image Product</label>
                            <input type="file" name="featured_image" id="featured_image" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="">Upload More Image Product</label>
                            <input type="file" id="more_image" name="more_image[]" class="form-control" onchange="preview_image();"
                                multiple required/>
                            <div id="image_preview"></div>
                        </div>
                        <div class="form-group">
                            <label for="">Description</label>
                            <textarea name="description" rows="30" id="description" class="form-control" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="">Stock</label>
                            <input type="number" name="stock" id="stock" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="">Price</label>
                            <input type="number" name="price" id="price" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-lg btn-primary" style="width: 100%;">Add
                                Product</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
function preview_image() {
    var total_file = document.getElementById("more_image").files.length;
    for (var i = 0; i < total_file; i++) {
        $('#image_preview').append("<img src='" + URL.createObjectURL(event.target.files[i]) + "' class='m-2' width='200' heigh='150'>");
    }
}
</script>
@endsection