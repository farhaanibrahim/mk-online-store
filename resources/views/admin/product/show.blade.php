@extends('admin.layout.master_layout')

@section('content')
<style>
    .description{
        white-space: pre-line;
        white-space: pre-wrap;
    }
</style>
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Products</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/admin/home">Home</a></li>
                    <li class="breadcrumb-item active">Products</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="row">
        <div class="col-6">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        Product Images
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        @foreach($product->images as $img)
                        <div class="col-sm-4">
                            <a href="#" data-toggle="lightbox" data-title="{{ $img->image }}" data-gallery="gallery">
                                <img src="{{ asset('uploads/products/'.$img->image) }}" class="img-fluid mb-2" />
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        Product Information
                    </div>
                </div>
                <div class="card-body">
                    <table class="table">
                        <tr>
                            <th>
                                Product Name
                            </th>
                            <td>
                                {{ $product->product_name }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Product Category
                            </th>
                            <td>
                                {{ $product->product_category }}
                            </td>
                        </tr>
                        <tr>
                            <th>Description</th>
                            <td><p class="description">{{ $product->description }}</p></td>
                        </tr>
                        <tr>
                            <th>Stock</th>
                            <td>{{ $product->stock }}</td>
                        </tr>
                        <tr>
                            <th>Price</th>
                            <td>Rp {{ $product->price }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection