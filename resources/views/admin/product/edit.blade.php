@extends('admin.layout.master_layout')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Edit Product</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/admin/home">Home</a></li>
                    <li class="breadcrumb-item active">Edit Product</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Products Data</h3>
                </div>
                <div class="card-body">
                    <form action="{{ URL::to('admin/product/update/'.$product->id) }}" method="post"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="">Product Name</label>
                            <input type="text" name="product_name" id="product_name"
                                value="{{ $product->product_name }}" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="">Product Category</label>
                            <select name="product_category" id="product_category" class="form-control" required>
                                <option value="">Select Product Category</option>
                                @foreach($categories as $category)
                                <option value="{{ $category->id }}"
                                    {{ $product->product_category_id == $category->id ? 'selected' : '' }}>
                                    {{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Featured Image Product</label>
                            <input type="file" name="featured_image" id="featured_image" class="form-control">
                            <input type="hidden" name="old_featured_image" value="{{ $product->image }}">
                            <p>Current Featured Image : </p>
                            <img src="{{ asset('uploads/products/'.$product->image) }}" width="200" height="150"
                                class="img img-thumbnail" alt="">
                        </div>
                        <div class="form-group">
                            <label for="">Upload More Image Product</label>
                            <input type="file" id="more_image" name="more_image[]" class="form-control"
                                onchange="preview_image();" multiple />
                            <div id="image_preview"></div>
                            <p>Current Product Images : </p>
                            <div class="row">
                            @foreach($product->images as $product_image)
                            <div class="m-2">
                                <img src="{{ asset('uploads/products/'.$product_image->image) }}" width="200"
                                        height="150" class="img img-thumbnail" alt="">
                                <div class="imgButton">
                                    <a href="{{ URL::to('remove-image/'.$product_image->id ) }}" class="btn btn-danger" style="width: 100%;">Remove</a>
                                </div>
                            </div>
                            @endforeach
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Description</label>
                            <textarea name="description" id="description" class="form-control" id="editor1" rows="30" style="height: 300px;" required>{{ $product->description }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="">Stock</label>
                            <input type="number" name="stock" id="stock" value="{{ $product->stock }}" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="">Price</label>
                            <input type="number" name="price" id="price" value="{{ $product->price }}" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-lg btn-primary" style="width: 100%;">Update
                                Product</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
function preview_image() {
    var total_file = document.getElementById("more_image").files.length;
    for (var i = 0; i < total_file; i++) {
        $('#image_preview').append("<img src='" + URL.createObjectURL(event.target.files[i]) +
            "' class='m-2' width='200' heigh='150'>");
    }
}
</script>
@endsection