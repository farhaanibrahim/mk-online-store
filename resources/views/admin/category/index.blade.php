@extends('admin.layout.master_layout')

@section('content')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Product Categories</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/admin/home">Home</a></li>
                    <li class="breadcrumb-item active">Product Categories</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Product Categories Data</h3>
                </div>
                <div class="card-body">
                    @if(Session::has('notification'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i
                                class="fa fa-times"></i></button>
                        <h5><i class="fa fa-check"></i> Info!</h5>
                        {{ Session::get('notification') }}
                    </div>
                    @endif
                    <a href="{{ URL::to('admin/product-categories/create') }}" class="btn btn-success mb-2"><i
                            class="fa fa-plus"></i> Add New Category</a>
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Category Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($categories as $i=>$category)
                            <tr>
                                <td>{{ $i+1 }}</td>
                                <td>{{ $category->name }}</td>
                                <td>
                                    <a href="{{ URL::to('admin/product-categories/edit/'.$category->id) }}"
                                        class="btn btn-warning"><i class="fa fa-pen"></i></a>
                                    <a href="{{ URL::to('admin/product-categories/destroy/'.$category->id) }}"
                                        class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection