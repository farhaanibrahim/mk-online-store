@extends('admin.layout.master_layout')



@section('content')

<section class="content-header">

    <div class="container-fluid">

        <div class="row mb-2">

            <div class="col-sm-6">

                <h1>Company Profile</h1>

            </div>

            <div class="col-sm-6">

                <ol class="breadcrumb float-sm-right">

                    <li class="breadcrumb-item"><a href="/admin/home">Home</a></li>

                    <li class="breadcrumb-item"><a href="/admin/product-categories">Profile Company</a></li>

                    <li class="breadcrumb-item active">Edit</li>

                </ol>

            </div>

        </div>

    </div><!-- /.container-fluid -->

</section>



<section class="content">

    <div class="row">

        <div class="col-12">

            <div class="card">

                <div class="card-header">

                    <h3 class="card-title">Company Profile</h3>

                </div>

                <div class="card-body">

                    @if(Session::has('notification'))

                    <div class="alert alert-success alert-dismissible">

                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i
                                class="fa fa-times"></i></button>

                        <h5><i class="fa fa-check"></i> Info!</h5>

                        {{ Session::get('notification') }}

                    </div>

                    @endif

                    <form action="{{ URL::to('admin/company-profile/update/') }}" method="post"
                        enctype="multipart/form-data">@csrf

                        <div class="form-group">

                            <label for="">Company Profile Content</label>

                            <textarea name="profile_content" id="profile_content" rows="20"
                                class="form-control">{{ $companyProfile->profile_content }}</textarea>

                        </div>

                        <div class="form-group">

                            <button type="submit" class="btn btn-primary">Save</button>

                        </div>

                    </form>

                </div>

            </div>

        </div>

    </div>

</section>

@endsection