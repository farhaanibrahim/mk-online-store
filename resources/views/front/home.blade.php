@extends('front.layout.master_layout')



@section('content')

<div class="container">

    <div class="row">


        <div class="col-lg-12">



            <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">

                <ol class="carousel-indicators">

                    @foreach($carousels as $i => $carousel)

                    <li data-target="#carouselExampleIndicators" data-slide-to="{{ $i }}"

                        class="{{ $i == 0 ? 'active' : '' }}"></li>

                    @endforeach

                </ol>

                <div class="carousel-inner" role="listbox">

                    @foreach($carousels as $i => $carousel)

                    <div class="carousel-item {{ $i == 0 ? 'active' : '' }}">

                        <img class="d-block img-fluid" src="{{ asset('uploads/carousels/'.$carousel->image) }}" alt="" style="width: 100%;">

                    </div>

                    @endforeach

                </div>

                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">

                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>

                    <span class="sr-only">Previous</span>

                </a>

                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">

                    <span class="carousel-control-next-icon" aria-hidden="true"></span>

                    <span class="sr-only">Next</span>

                </a>

            </div>



            <div class="row">



                @foreach($products as $product)

                <div class="col-lg-4 col-md-6 mb-4">

                    <div class="card h-100">

                        <a href="{{ URL::to('product/'.$product->category_slug.'/'.$product->slug) }}"><img class="card-img-top"

                                src="{{ asset('uploads/products/'.$product->image) }}" width="700" height="250" style="overflow: hidden;" alt=""></a>

                        <div class="card-body">

                            <h4 class="card-title">

                                <a

                                    href="{{ URL::to('product/'.$product->category_slug.'/'.$product->slug) }}">{{ $product->product_name }}</a>

                            </h4>

                            <!-- <h5>Rp {{ $product->price }}</h5> -->

                            <p class="card-text">{{ substr($product->description, 0, 200) }} ... <a href="{{ URL::to('product/'.$product->category_slug.'/'.$product->slug) }}">More</a></p>

                        </div>

                    </div>

                </div>

                @endforeach

                <div class="col-lg-12">
                {{ $products->links() }}
                </div>

            </div>

            <!-- /.row -->



        </div>

    </div>

</div>

@endsection