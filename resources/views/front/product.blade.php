@extends('front.layout.master_layout')



@section('content')

<style>

img {

    max-width: 100%;

}



.preview {

    display: -webkit-box;

    display: -webkit-flex;

    display: -ms-flexbox;

    display: flex;

    -webkit-box-orient: vertical;

    -webkit-box-direction: normal;

    -webkit-flex-direction: column;

    -ms-flex-direction: column;

    flex-direction: column;

}



@media screen and (max-width: 996px) {

    .preview {

        margin-bottom: 20px;

    }

}



.preview-pic {

    -webkit-box-flex: 1;

    -webkit-flex-grow: 1;

    -ms-flex-positive: 1;

    flex-grow: 1;

}



.preview-thumbnail.nav-tabs {

    border: none;

    margin-top: 15px;

}



.preview-thumbnail.nav-tabs li {

    width: 18%;

    margin-right: 2.5%;

}



.preview-thumbnail.nav-tabs li img {

    max-width: 100%;

    display: block;

}



.preview-thumbnail.nav-tabs li a {

    padding: 0;

    margin: 0;

}



.preview-thumbnail.nav-tabs li:last-of-type {

    margin-right: 0;

}



.tab-content {

    overflow: hidden;

}



.tab-content img {

    width: 100%;

    -webkit-animation-name: opacity;

    animation-name: opacity;

    -webkit-animation-duration: .3s;

    animation-duration: .3s;

}



.card {

    margin-top: 100px;

    margin-bottom: 50px;

    padding: 3em;

    line-height: 1.5em;

}



@media screen and (min-width: 997px) {

    .wrapper {

        display: -webkit-box;

        display: -webkit-flex;

        display: -ms-flexbox;

        display: flex;

    }

}



.details {

    display: -webkit-box;

    display: -webkit-flex;

    display: -ms-flexbox;

    display: flex;

    -webkit-box-orient: vertical;

    -webkit-box-direction: normal;

    -webkit-flex-direction: column;

    -ms-flex-direction: column;

    flex-direction: column;

}



.colors {

    -webkit-box-flex: 1;

    -webkit-flex-grow: 1;

    -ms-flex-positive: 1;

    flex-grow: 1;

}



.product-title,

.price,

.sizes,

.colors {

    text-transform: UPPERCASE;

    font-weight: bold;

}



.checked,

.price span {

    color: #ff9f1a;

}



.product-title,

.rating,

.product-description,

.price,

.vote,

.sizes {

    margin-bottom: 15px;

}



.product-title {

    margin-top: 0;

}



.size {

    margin-right: 10px;

}



.size:first-of-type {

    margin-left: 40px;

}



.color {

    display: inline-block;

    vertical-align: middle;

    margin-right: 10px;

    height: 2em;

    width: 2em;

    border-radius: 2px;

}



.color:first-of-type {

    margin-left: 20px;

}



.add-to-cart,



.add-to-cart:hover,

.like:hover {

    background: #b36800;

    color: #fff;

}



.not-available {

    text-align: center;

    line-height: 2em;

}



.not-available:before {

    font-family: fontawesome;

    content: "\f00d";

    color: #fff;

}



.orange {

    background: #ff9f1a;

}



.green {

    background: #85ad00;

}



.blue {

    background: #0076ad;

}



.tooltip-inner {

    padding: 1.3em;

}



@-webkit-keyframes opacity {

    0% {

        opacity: 0;

        -webkit-transform: scale(3);

        transform: scale(3);

    }



    100% {

        opacity: 1;

        -webkit-transform: scale(1);

        transform: scale(1);

    }

}



@keyframes opacity {

    0% {

        opacity: 0;

        -webkit-transform: scale(3);

        transform: scale(3);

    }



    100% {

        opacity: 1;

        -webkit-transform: scale(1);

        transform: scale(1);

    }

}



/*# sourceMappingURL=style.css.map */

</style>

<style>

    .description{

        white-space: pre-line;

        white-space: pre-wrap;

    }

</style>

<div class="container-fluid">

    <div class="row">

        <div class="col-lg-12">

            <div class="card">

                <div class="container-fluid">

                    <div class="wrapper row">

                        <div class="preview col-md-6">



                            <div class="preview-pic tab-content">

                                <div class="tab-pane active" id="pic-featured"><img

                                        src="{{ asset('uploads/products/'.$product->image) }}" />

                                </div>

                                @foreach($product->images as $i=>$image)

                                <div class="tab-pane" id="pic-{{ $i+1 }}"><img

                                        src="{{ asset('uploads/products/'.$image->image) }}" /></div>

                                @endforeach

                            </div>

                            <ul class="preview-thumbnail nav nav-tabs">

                                <li class="active"><a data-target="#pic-featured" data-toggle="tab"><img

                                            src="{{ asset('uploads/products/'.$product->image) }}" /></a></li>

                                @foreach($product->images as $i=>$image)

                                <li><a data-target="#pic-{{ $i+1 }}" data-toggle="tab"><img

                                            src="{{ asset('uploads/products/'.$image->image) }}" /></a></li>

                                @endforeach

                            </ul>



                        </div>

                        <div class="details col-md-6">

                            <h3 class="product-title">{{ $product->product_name }}</h3>

                            <div class="rating">

                                <div class="stars">

                                    <span class="fa fa-star checked"></span>

                                    <span class="fa fa-star checked"></span>

                                    <span class="fa fa-star checked"></span>

                                    <span class="fa fa-star"></span>

                                    <span class="fa fa-star"></span>

                                </div>

                                <!-- <span class="review-no">41 reviews</span> -->

                            </div>

                            <p class="product-description description">

                                {{ $product->description }}

                            </p>

                            <!-- <h4 class="price">current price: <span>Rp {{ $product->price }}</span></h4> -->



                            <div class="action">

                                <h5>To make a purchase, please contact the following number</h5>

                                <a href="https://wa.me/+6281395751279?text={{ URL::current() }}%0aSaya%20tertarik%20dengan%20produk%20yang anda%20jual"><img src="{{ asset('assets/front/img/whatsapp.png') }}" width="50" class="d-inline"><span class="d-inline"><h6>+62 813-9575-1279</h6></span></a>

                                <a href="https://wa.me/+6281310066819?text={{ URL::current() }}%0aSaya%20tertarik%20dengan%20produk%20yang anda%20jual"><img src="{{ asset('assets/front/img/whatsapp.png') }}" width="50" class="d-inline"><span class="d-inline"><h6>+62 813-1006-6819</h6></span></a>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

@endsection