@extends('front.layout.master_layout')



@section('content')

<div class="container">

    <div class="row">

        <div class="col-lg-12">



            <h1 class="my-4">{{ $category_name }}</h1>

            @if(count($products) == 0)
            <div class="alert alert-success" role="alert">
                Barang Belum Tersedia
            </div>
            @endif

            <div class="row">

                @foreach($products as $product)

                <div class="col-lg-4 col-md-6 mb-4">

                    <div class="card h-100">

                        <a href="{{ URL::to('product/'.$product->category_slug.'/'.$product->slug) }}"><img
                                class="card-img-top" src="{{ asset('uploads/products/'.$product->image) }}" width="700"
                                height="250" alt=""></a>

                        <div class="card-body">

                            <h4 class="card-title">

                                <a
                                    href="{{ URL::to('product/'.$product->category_slug.'/'.$product->slug) }}">{{ $product->product_name }}</a>

                            </h4>

                            <!-- <h5>Rp {{ $product->price }}</h5> -->

                            <p class="card-text">{{ substr($product->description, 0, 200) }} ... <a href="{{ URL::to('product/'.$product->category_slug.'/'.$product->slug) }}">More</a></p>

                        </div>

                    </div>

                </div>

                @endforeach

                <div class="col-lg-12">
                {{ $products->links() }}
                </div>

            </div>

            <!-- /.row -->



        </div>

    </div>

</div>

@endsection