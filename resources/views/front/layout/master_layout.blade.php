<!DOCTYPE html>

<html lang="en">



<head>



    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="description" content="">

    <meta name="author" content="">



    <title>TOKO PEDIAILMU</title>



    <link rel="icon" href="{{ asset('assets/front/img/logo.png') }}">

    

    <!-- Bootstrap core CSS -->

    <link href="{{ asset('assets/front/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">



    <!-- Custom styles for this template -->

    <link href="{{ asset('assets/front/css/shop-homepage.css') }}" rel="stylesheet">

    <!-- Font Awesome -->

    <link rel="stylesheet" href="{{ asset('assets/front/css/fontawesome-free/css/all.min.css') }}">

</head>



<body>



    @include('front.layout.header')

    <!-- Navigation -->



    <!-- Page Content -->

    @yield('content')



    @include('front.layout.footer')





    <!-- Bootstrap core JavaScript -->

    <script src="{{ asset('assets/front/vendor/jquery/jquery.min.js') }}"></script>

    <script src="{{ asset('assets/front/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>



</body>



</html>