<h1 class="my-4" style="font-family: 'Arial'; font-size: 30px;">TOKO PEDIAILMU </h1>
<p class="" style="font-family: 'Arial';">TOKO BUKU & STATIONARY</p>

<div class="list-group">

    <form action="{{ URL::to('product/search') }}" method="post" enctype="multipart/form-data">
        @csrf
        <input type="text" name="search" id="search" class="form-control form-control-lg mb-2" placeholder="Search">
    </form>

    @foreach($categories as $category)

    <a href="{{ URL::to('product/'.$category->slug) }}" class="list-group-item">{{ $category->name }}</a>

    @endforeach

</div>