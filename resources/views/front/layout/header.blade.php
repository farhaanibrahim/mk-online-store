<style>
.custom-background {
    background: #FF6600;
}
</style>
<nav class="navbar navbar-expand-lg navbar-dark custom-background">

    <div class="container">

        <a class="navbar-brand" href="{{ URL::to('/') }}"><img src="{{ asset('assets/front/img/logo.png') }}"
                style="width: 10%;" alt=""> Toko Buku, Stationary & Kertas</a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
            aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">

            <span class="navbar-toggler-icon"></span>

        </button>

        <div class="collapse navbar-collapse" id="navbarResponsive">

            <ul class="navbar-nav ml-auto">

                <li class="nav-item active">

                    <a class="nav-link" href="{{ URL::to('/') }}">Home

                        <span class="sr-only">(current)</span>

                    </a>

                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Categories
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        @foreach($categories as $category)

                        <a href="{{ URL::to('product/'.$category->slug) }}"
                            class="dropdown-item">{{ $category->name }}</a>

                        @endforeach
                    </div>
                </li>

                <li class="nav-item">

                    <a class="nav-link" href="{{ URL::to('profile') }}">Profile</a>

                </li>

                <form class="form-inline my-2 my-lg-0" action="{{ URL::to('product/search') }}" method="post" enctype="multipart/form-data">@csrf
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <!-- <button class="btn btn-success my-2 my-sm-0" type="submit">Search</button> -->
                </form>

            </ul>

        </div>

    </div>

</nav>

<div class="bottom-border gradient">



</div>