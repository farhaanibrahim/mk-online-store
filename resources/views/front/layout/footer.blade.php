<footer class="py-5 bg-dark">

    <div class="container">

        <div class="row">
            <div class="col-md-4 text-white">
                <p>Copyright &copy; Toko Pediailmu 2020</p>
                <img src="{{ asset('assets/front/img/logo.png') }}" class="img-fluid" style="width: 30%;" alt="">
            </div>
            <div class="col-md-4 text-white">
                <h4>Contact</h4>
                <p>0251 - 7542 - 386</p>
                <p>0813 - 1006 - 6819</p>
                <p>0813 - 9575 - 1279</p>
            </div>
            <div class="col-md-4 text-white">
                <h4>Email</h4>
                <p>tokopediailmu@gmail.com</p>
                <a href="#"><img src="https://img.icons8.com/color/48/000000/facebook-circled.png"/></a>
                <a href="https://www.instagram.com/toko_pediailmu/"><img src="https://img.icons8.com/officel/40/000000/instagram-new.png"/></a>
                <a href="#"><img src="https://img.icons8.com/color/48/000000/twitter-circled.png"/></a>
            </div>
        </div>


    </div>

    <!-- /.container -->

</footer>

<div class="gradient gradient-bottom">



</div>