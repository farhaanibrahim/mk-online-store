<?php



use Illuminate\Support\Facades\Route;



/*

|--------------------------------------------------------------------------

| Web Routes

|--------------------------------------------------------------------------

|

| Here is where you can register web routes for your application. These

| routes are loaded by the RouteServiceProvider within a group which

| contains the "web" middleware group. Now create something great!

|

*/



Route::get('/', 'HomeController@index');

Route::get('profile', 'HomeController@profile');

Route::get('product/{category_slug}/{product_slug}', 'ProductController@show2');

Route::get('product/{category_slug}', 'ProductController@showProductByCategory');

Route::post('product/search', 'ProductController@search');



Route::get('admin/login', 'AuthController@admin_login_view');

Route::post('admin/login', 'AuthController@admin_login_process');

Route::get('admin/logout', 'AuthController@admin_logout');



Route::get('admin/home', 'HomeAdminController@index');



Route::get('admin/product-categories', 'CategoryController@index');

Route::get('admin/product-categories/create', 'CategoryController@create');

Route::post('admin/product-categories/store', 'CategoryController@store');

Route::get('admin/product-categories/edit/{id}', 'CategoryController@edit');

Route::post('admin/product-categories/update/{id}', 'CategoryController@update');

Route::get('admin/product-categories/destroy/{id}', 'CategoryController@destroy');



Route::get('admin/product', 'ProductController@index');

Route::get('admin/product/create', 'ProductController@create');

Route::post('admin/product/store', 'ProductController@store');

Route::get('admin/product/show/{id}', 'ProductController@show');

Route::get('admin/product/edit/{id}', 'ProductController@edit');

Route::post('admin/product/update/{id}', 'ProductController@update');

Route::get('admin/product/destroy/{id}', 'ProductController@destroy');



Route::get('remove-image/{id}', 'ProductController@remove_image');



Route::get('admin/carousel', 'CarouselController@index');

Route::get('admin/carousel/create', 'CarouselController@create');

Route::post('admin/carousel/store', 'CarouselController@store');

Route::get('admin/carousel/edit/{id}', 'CarouselController@edit');

Route::post('admin/carousel/update/{id}', 'CarouselController@update');

Route::get('admin/carousel/destroy/{id}', 'CarouselController@destroy');